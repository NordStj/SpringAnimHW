//
//  ViewController.swift
//  SpringAnimHW
//
//  Created by Дмитрий on 26.07.2023.
//

import UIKit
import SpringAnimation

class ViewController: UIViewController {
    
    
   
    @IBOutlet var showView: SpringView!
    @IBOutlet var runButton: UIButton!
    
    
    @IBOutlet var presentLabel: UILabel!
    @IBOutlet var curveLabel: UILabel!
    @IBOutlet var forceLabel: UILabel!
    @IBOutlet var durationLabel: UILabel!
    @IBOutlet var delayLabel: UILabel!
    
    private var animation = DataManager.shared.getAnimation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startApp()
    }
   
    @IBAction func useButton(_ sender: Any) {
        setupSettings()
        showView.animate()
        setupLabel()
        getAnimation()
        setupTextButton()
    }
    
}
extension ViewController {
    
    private func setupLabel() {
        presentLabel.text = "present: \(animation.present)"
        curveLabel.text = "curve: \(animation.curve)"
        forceLabel.text = "force: \(String(format: "%.1f", animation.force))"
        durationLabel.text = "duration: \(String(format: "%.1f", animation.duration))"
        delayLabel.text = "delay: \(String(format: "%.1f", animation.delay))"
    }
    
    private func setupSettings() {
        showView.animation = animation.present
        showView.curve = animation.curve
        showView.duration = animation.duration
        showView.force = animation.force
        showView.delay = animation.delay
    }
    
    private func getAnimation() {
        animation = DataManager.shared.getAnimation()
    }
    
    private func setupTextButton() {
        runButton.setTitle("Run \(animation.present)", for: .normal)
    }
    
    private func startApp() {
        showView.layer.cornerRadius = 10
        runButton.layer.cornerRadius = 10
        setupLabel()
    }
    
}

