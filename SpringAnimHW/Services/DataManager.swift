//
//  DataManager.swift
//  SpringAnimHW
//
//  Created by Дмитрий on 26.07.2023.
//


import SpringAnimation
import Foundation

final class DataManager {
    
    static let shared = DataManager()
    
    private init() {}
    
    func getAnimation() -> Animation {
        
        guard let animation = AnimationPreset.allCases.randomElement()?.rawValue,
              let curve = AnimationCurve.allCases.randomElement()?.rawValue
        else {return Animation(present: "pop", curve: "easeIn", force: 0.0, duration: 0.0, delay: 0.0)}
        
        let force = CGFloat.random(in: 1...5)
        let duration = CGFloat.random(in: 0.5...5)
        let delay = CGFloat.random(in: 0.3...0.9)
        
        return Animation(
            present: animation,
            curve: curve,
            force: force,
            duration: duration,
            delay: delay)
    }
}
