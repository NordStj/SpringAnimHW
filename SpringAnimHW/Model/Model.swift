//
//  Model.swift
//  SpringAnimHW
//
//  Created by Дмитрий on 26.07.2023.
//
import Foundation

struct Animation {
    let present: String
    let curve: String
    let force: CGFloat
    let duration: CGFloat
    let delay: CGFloat
}
